package main.java.aggregators;

import main.java.fileprocessors.StockFileReader;

import java.io.IOException;
import java.util.List;

public class AggregatorProcessor <T extends Aggregator> {
    T aggregator;
    String file;

    public AggregatorProcessor(T aggregator, String file) {
        this.aggregator = aggregator;
        this.file = file;
    }

    public double runAggregator(int columId) throws IOException {
        StockFileReader fileReader = new StockFileReader(file);
        List<String> fileData = fileReader.readFileData();
        columId--;

        for(String data: fileData) {
            String[] numbers = data.split(",");
            Number value = Double.parseDouble(numbers[columId]);
            aggregator.add(value.doubleValue());
        }

        return  aggregator.calculate();
    }
}
